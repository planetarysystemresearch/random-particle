# README #

'Random particles' packages is for creating and manipulating single random particle shapes. The shapes include currently Gaussian spheres, Gaussian ellipsoids, random convex polyhedra and random Voronoi polyhedra.

### How do I get set up? ###

* You need some Python packages, especially pymesh. Needs to be installed 'manually', not just using 'pip'. Installs quite nicely on Linux, nightmare to get working on Windows. For Mac, I don't know.
* For Voronoi particles you need also [QHull code](http://www.qhull.org/) compiled and executables in your system's search path.

### Who do I talk to? ###

* antti.i.penttila a t helsinki.fi
