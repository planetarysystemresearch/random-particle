def to_volume_grid(mesh, res):
	"""
	Voxelizes the surface mesh into cubical voxels with give size.
	
	Input:
		mesh : Pymesh surface mesh object to be voxelized
		res : Sidelength of the cubical voxel
	
	Output:
		Numpy n*3 matrix of x,y,z integer coordinates of the cubical mesh
	"""

	mini = np.int32(np.floor(mesh.bbox[0]/res))
	maxi = np.int32(np.ceil(mesh.bbox[1]/res))
	ni = maxi-mini+1
	print(" [volume grid is "+str(ni[0])+"x"+str(ni[1])+"x"+str(ni[2])+" voxels]")

	# Triangles projected to 2d
	tri2d = [mesh.vertices[tri][:,[1,2]] for tri in mesh.faces]

	# For collecting intersecting triangles for y,z grid
	tribufgrid = [[[] for j in range(ni[2])] for i in range(ni[1])]
	# For building zbuffer for each yz grid point
	zbufgrid = [[[] for j in range(ni[2])] for i in range(ni[1])]
	udir = np.array([1,0,0])
	minx = mini[0]*res
	# For building voxel list
	vox = []
	xran = list(range(ni[0]))
	xpen = np.array(xran) * res
	ones = np.ones(ni[0])
	
	# Main loop over yz-grid along x-axis
	for i in range(ni[1]):
		y = (i+mini[1])*res
		if(i%5 == 0):
			print(" [now in y-index "+str(i)+"/"+str(ni[1])+"]")

		for j in range(ni[2]):
			yz = np.array([y,(j+mini[2])*res])
			xyz = np.array([minx,y,(j+mini[2])*res])
			bufsum = np.zeros(ni[0],dtype=np.int8)
			
			# Collect intersecting triangles for y,z grid
			for tri,[a,b,c] in enumerate(tri2d):
				if(_point_in_triangle_2d(yz, a, b, c)):
					tribufgrid[i][j].append(tri)

			# Build zbuffer for each yz grid point
			for pind in tribufgrid[i][j]:
				vind = mesh.faces[pind]
				poly = mesh.vertices[vind]
				zbufgrid[i][j].append(_intersect_triangle_3d(xyz, udir, poly[0], poly[1], poly[2]))
			zbufgrid[i][j] = np.unique(np.array(zbufgrid[i][j]).round(decimals=8)).tolist()

			# Build voxel list
			for zval in zbufgrid[i][j]:
				bufsum += np.array([int(b) for b in xpen>zval])
			xis = list(compress(xran,bufsum % 2 == 1))
			for x in xis:
				vox.append([x,i,j])
	
	return np.array(vox)

#END to_volume_grid

def _intersect_triangle_3d(ori, dir, a, b, c):
	"""
	Find the intersection point of line and triangle in 3d.
	
	Modified from Möller and Trumbore algorithm by BrunoLevy in StackOverflow.
	NOTE, this version does not check if the line actually intersects but trusts that
	it has been checked before.
	
	Input:
		ori Origin of the line
		dir Direction of the line
		a,b,c Vertices of the triangle
	
	Output:
		Distance c that gives the intersection point as ori + c*dir
	"""

	n = np.cross(b-a,c-a)
	det = -np.dot(dir, n)
	t =  np.dot(ori-a,n) / det 

	return t

#END _intersect_triangle_3d




def _same_side_2d(p1, p2, a, b):
	"""
	Check if points p1 and p2 are on the same side of
	the line from p1 to p2
	"""
	cp1 = np.cross(b-a, p1-a)
	cp2 = np.cross(b-a, p2-a)
	if(np.dot(cp1, cp2) >= 0):
		return True
	else:
		return False

#END _same_side_2d



def _point_in_triangle_2d(p, a, b, c):
	if (_same_side_2d(p,a,b,c) and _same_side_2d(p,b,a,c) and _same_side_2d(p,c,a,b)):
		return True
	else:
		return False

#END _point_in_triangle

