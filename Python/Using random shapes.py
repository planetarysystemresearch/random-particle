#!/usr/bin/env python
# coding: utf-8

# # Random shapes package, usage guide

# ## Imports

# In[1]:


import sys
sys.path.append("../Python/")


# In[2]:


from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "all"


# In[3]:


import numpy as np
get_ipython().run_line_magic('matplotlib', 'notebook')
from matplotlib import pyplot as plt
import pymesh


# In[4]:


import random_shapes as rs


# In[5]:


import importlib
importlib.reload(rs)


# ## Gaussian sphere

# Create one Gaussian sphere particle as surface mesh.

# In[13]:


help(rs.generate_gsphere)


# In[20]:


gsphere = rs.generate_gsphere()
rs.plot_mesh(gsphere)


# Show what's in gsphere object.

# In[21]:


print(gsphere.vertices)
print(gsphere.faces)
print(gsphere.num_vertices)
print(gsphere.num_faces)
print(gsphere.dim)
print(gsphere.vertex_per_face)
print(gsphere.bbox)


# ## Gaussian ellipsoid

# Create one Gaussian ellipsoid.

# In[8]:


help(rs.generate_gellipsoid)


# In[9]:


gellip = rs.generate_gellipsoid()
rs.plot_mesh(gellip)


# Show what's in the object.

# In[10]:


print(gellip.vertices)
print(gellip.faces)
print(gellip.num_vertices)
print(gellip.num_faces)
print(gellip.dim)
print(gellip.vertex_per_face)
print(gellip.bbox)


# ## Random polyhedra

# Generate one random convex polyhedral particle.

# In[11]:


help(rs.generate_convex_polyhedra)


# In[12]:


ch = rs.generate_convex_polyhedra(30)
rs.plot_mesh(ch)


# In[13]:


print(ch.vertices)
print(ch.faces)
print(ch.num_vertices)
print(ch.num_faces)
print(ch.dim)
print(ch.vertex_per_face)
print(ch.bbox)


# ## Random voronoi polyherda

# Generate a collection of Voronoi particles.

# In[6]:


help(rs.generate_voronoi_polyhedra)


# In[11]:


ch = rs.generate_voronoi_polyhedra(60)
rs.plot_mesh(ch[0])


# In[12]:


print(ch[0].vertices)
print(ch[0].faces)
print(ch[0].num_vertices)
print(ch[0].num_faces)
print(ch[0].dim)
print(ch[0].vertex_per_face)
print(ch[0].bbox)


# ## Sphere shape

# Generate iconosphere shape.

# In[8]:


help(rs.generate_sphere)


# In[22]:


sphere = rs.generate_sphere()
rs.plot_mesh(sphere)


# ## Scale and/or translate mesh if needed

# In[14]:


help(rs.scale_mesh)


# In[15]:


mesh_scaled = rs.scale_mesh(gsphere, 0.5)
rs.plot_mesh(mesh_scaled)


# ## Area and volume

# Get mesh particle's area and volume. Volume computation also discretazes the particle into tetrahedral mesh.

# In[25]:


# Area
4*np.pi
rs.area(sphere)
rs.area(gsphere)


# In[31]:


# Volume
4/3*np.pi
[vol, vsphere] = rs.volume(sphere)
vol
[vol, vgsphere] = rs.volume(gsphere)
vol


# ## Export shapes

# The surface mesh can be saved directly in .obj or .off formats using pymesh functions.

# In[16]:


pymesh.save_mesh("gsphere.obj", gsphere)
pymesh.save_mesh("gsphere.off", gsphere)
pymesh.save_mesh("gellip.obj", gellip)
pymesh.save_mesh("gellip.off", gellip)


# ## Converting into volume grid of cubical voxels

# In[17]:


help(rs.to_volume_grid)


# In[18]:


vox = rs.to_volume_grid(gsphere, 0.1)
len(vox)


# Then save to ADDA file.

# In[19]:


help(rs.save_volume_to_adda)


# In[20]:


rs.save_volume_to_adda(vox)

