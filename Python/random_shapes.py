"""
Package for creating random particle shapes, now implementing
Gaussian spheres and ellipsoids.

Heavily based on 'generate_gsphere' and 'generate_gellip' Python
packages in 'scadyn' software by Joonas Herranen. The Gaussian sphere
and spheroid shapes are based on the works by Karri Muinonen et al.

Needs all the imports below, especially pymesh.

Written by Antti Penttilä, 2022, major parts from collection by Joonas Herranen.
"""

import subprocess

from itertools import compress

import numpy as np
from numpy.random import seed, normal
from numpy import linalg as la
from numpy.linalg import norm

from matplotlib import pyplot as plt

from scipy.special import spherical_in, factorial, lpmn
from scipy.spatial import ConvexHull as ch

from mpl_toolkits import mplot3d

import pymesh


###############################################
# Public functions


def area(mesh):
	"""
	Computes area of the mesh particle.
	
	Input:
		mesh : Surface mesh
	
	Output:
		area
	"""
	
	mesh.add_attribute("face_area")
	
	return np.sum(mesh.get_attribute("face_area"))

#END area


def generate_convex_polyhedra(n, b=1.0, c=1.0, rseed=None):
	"""
	Generate random convex polyhedral particle.
	
	Input parameters:
		n : Number of vertex points in the particle
		b (1.0) : Scaling for b-axis (a is 1.0)
		c (1.0) : Scaling for c-axis (a is 1.0)
		rseed (None) : Seed for randon number generator
		
	Result:
		Convex polyhedral particle surface mesh. . Has attributes
		vertices, faces, num_vertices, num_faces, dim, vertex_per_face, bbox
	
	Note, using scalings b and c will just introduce a linear scaling to 
	points that are uniform on a unit sphere. Thus, triangles on scaled axis
	will get scaled, and the triangles will not be uniform in size distribution
	anymore after scaling.
	"""

	if(isinstance(rseed,int)):
		seed(rseed)

	# Initial random directions
	phi = np.random.uniform(0,np.pi*2,n)
	sinphi = np.sin(phi)
	cosphi = np.cos(phi)
	costheta = np.random.uniform(-1,1,n)
	sintheta = np.sqrt(1.0 - costheta**2)
	
	# To cartesian vectors
	uv = np.array([sintheta * cosphi, b * sintheta * sinphi, c * costheta]).transpose()
	
	# Form mockup mesh and get its convex hull
	mesh=pymesh.form_mesh(uv, faces=np.array([[0,1,2]]))
	ch = pymesh.convex_hull(mesh)
	
	print(" [polyhedra has "+str(len(ch.vertices))+" vertices and "+str(len(ch.faces))+" triangles]")
	
	return ch

#END generate_convex_polyhedra


def generate_gellipsoid(sigma=0.1, b=0.8, c=0.5, ell=0.35, n=200, rseed=None):
	"""
	Generate gaussian ellipsoid sample.
	
	Input parameteres:
		sigma (0.1) : Relative std of radial distance
		b (0.8) : Second ellipsoid semiaxis
		c (0.5) : Third ellipsoid semiaxis
		ell (0.35) : Correlation length in the Gaussian correlation function
		n (200) : Number of sphere points
		rseed (None) : Seed for random number generator
	
	Result:
		Gaussian ellipsoid surface mesh (pymesh object). Has attributes
		vertices, faces, num_vertices, num_faces, dim, vertex_per_face, bbox
	"""
	
	a = 1.0
	h = a*(c/a)**2
	sigma = sigma/h
	beta = np.sqrt(np.log(sigma**2+1.0))
	
	if(isinstance(rseed,int)):
		seed(rseed)
	
	ellipsoid = _genellip(a, b, c, n)
	
	# Discrete triangle representation for a sample G-sphere.
	node = _deform_mesh(ellipsoid, ell, h, beta)
	gellip = pymesh.form_mesh(node,ellipsoid.elements)
	gellip = _fix_mesh(gellip,detail="low")
 
	return gellip

#END generate_gellipsoid


def generate_gsphere(sigma=0.1, cflg=1, lmin=2, lmax=7, nu=3.0, gamma=35.0, \
	rseed=None, shape_order=3):
	"""
	Generate gaussian sphere sample.
	
	Input parameters:
		sigma (0.1) : Relative std of radial distance
		cflg (1) : Correlation function C_1 = power law, C_2 = Gauss
		lmin (2) : Minimum degree in C_1, C_2
		lmin (7) : Maximum degree in C_1, C_2
		nu (3.0) : Power law index for C_1 correlation
		gamma (35.0) : Input angle for C_2 correlation
		rseed (None) : Seed for random number generator
		shape_order (3) : Refinement order for the underlying iconosphere.
			Number of facets is 20*(4**shape_order)
	
	Result:
		Gaussian sphere surface mesh (pymesh object). Has attributes
		vertices, faces, num_vertices, num_faces, dim, vertex_per_face, bbox
	"""

	gamma = gamma*np.pi/180
	ell = 2.0*np.sin(0.5*gamma)
	beta = np.sqrt(np.log(sigma**2+1.0))
	
	cind = np.arange(lmax*(lmax+1)/2, dtype=int)
	l, m = _c_ind(cind)

	# Autocorrelation
	if(cflg == 1):
		a_l = _power_lcoef(l, nu, lmin, lmax)
	if(cflg == 2):
		a_l = _mgaussian_lcoef(l, ell)

	if(isinstance(rseed,int)):
		seed(rseed)
	std = _coef_std(a_l, beta, l, m, cind)

	# Generate a sample Gaussian sphere with id gid, then move to discretization and output
	sphere = pymesh.generate_icosphere(1.0,[0.,0.,0.],shape_order)
	a_lm, b_lm = _sample_gaussian_sphere_coef(std, l, lmin, lmax)
	new_vertices = _deform(sphere, a_lm, b_lm, beta, lmin, lmax)
	gsphere = pymesh.form_mesh(new_vertices, sphere.elements)
	
	return gsphere

#END generate_gsphere


def generate_voronoi_polyhedra(initk, rseed=None):
	"""
	Generate random polyhedral particles based on Voronoi division.
	
	Input parameters:
		initk : Number initial Voronoi cell seed points
		rseed (None) : Seed for randon number generator
		
	Result:
		List of Voronoi convex polyhedral particle surface meshes. Each mesh
		has attributes vertices, faces, num_vertices, num_faces, dim,
		vertex_per_face, bbox
	
	Note, will produde a number of particles  between 0 and initk. Particle
	sizes are not scaled, but all vertices in all particles should have distance
	from (0,0,0) smaller than 2.
	"""
	
	if(isinstance(rseed,int)):
		seed(rseed)
	
	rc = _sample_from_sphere(n=initk, r=2.0)
	with open("temp.dat","w") as f:
		f.write("3\n")
		f.write(str(initk)+"\n")
		for [x,y,z] in rc:
			f.write(str(x)+" "+str(y)+" "+str(z)+"\n")
		f.close()
	subprocess.run("qvoronoi TI temp.dat o TO temp.off", shell=True)
	with open("temp.off","r") as f:
		d,nv,nf,t = np.fromfile(f, dtype=int, count=4, sep=" ")
		ve = np.fromfile(f, dtype=float, count=d*nv, sep=" ").reshape(nv,d)
		fa = [np.array(list(map(int,f.readline().split()))) for i in range(nf)]
		f.close()
	sf = [f for f,v in zip(fa,rc) if np.linalg.norm(v)<=1 and np.min(f) > 0]
	sv = [ve[fa] for fa in sf if max(list(map(np.linalg.norm,ve[fa]))) < 2]
	chc = [pymesh.convex_hull(pymesh.meshio.form_mesh(v,np.array([[0,1,2]]))) for v in sv]
	
	print(" [Harvested "+str(len(chc))+" voronoi particles]")
	
	return chc

#END generate_voronoi_polyhedra


def plot_mesh(mesh):
	"""
	Plots the surface mesh using pyplot and mplot3d.
	"""
	
	fig = plt.figure(figsize=(4,4))
	ax = mplot3d.Axes3D(fig, auto_add_to_figure=False)
	fig.add_axes(ax)

	meshvectors = _mesh_vectors(mesh.vertices, mesh.faces)
	ax.add_collection3d(mplot3d.art3d.Poly3DCollection(meshvectors, facecolor=[0.5,0.5,0.5], lw=0.5, edgecolor=[0,0,0], alpha=.8, antialiaseds=True))  
	scale = mesh.vertices.flatten('F')
	ax.auto_scale_xyz(scale, scale, scale)
	ax.set_xlabel('x')
	ax.set_ylabel('y')
	ax.set_zlabel('z')
	
	plt.show()
	 
	return

#END plot_mesh


def save_volume_to_adda(vox, fname='voxels.geom', center=False):
	"""
	Saves volume grid in ADDA format.
	
	Input:
		vox : Voxel list
		fname ('voxels.geom') : Name of the file
		center (False) : Boolean swith if to translate dipole coordinates
			to have mean of (0,0,0)
	"""
	
	if(center):
		mean = np.rint(np.mean(vox,axis=0)).astype(np.int32)
		vox1 = vox-mean
	else:
		vox1 = vox
	
	with open(fname, "w") as f:
		
		f.write("# Volume written from 'random_particles.py', n = "+str(len(vox1))+"\n")
		for [x,y,z] in vox1:
			f.write(str(x)+" "+str(y)+" "+str(z)+"\n")
		
		f.close()
	
#END save_volume_to_adda


def scale_mesh(mesh, sca, center=None):
	"""
	Scales the mesh by given factor. Can center the mesh
	before scaling.
	
	Input:
		mesh : Mesh to be scaled.
		sca : Scaling factor which multiplies the dimensions.
		center (True) : If 'True', translate the mesh to its center given by the mean
			of the vertices before scaling. If list of 3 numbers, translate with that vector.
	
	Output:
		Scaled pymesh object.
	"""
	
	if(isinstance(center,bool) and center):
		ori = np.mean(mesh.vertices)
	elif(isinstance(center,(list, tuple, np.ndarray)) and len(center)==3):
		ori = center
	else:
		ori = [0,0,0]
	
	vert = mesh.vertices + ori
	vert *= sca
	
	return pymesh.form_mesh(vert, mesh.faces)

#END scale_mesh


def generate_sphere(shape_order=3):
	"""
	Generate sphere (iconosphere) shape.
	
	Input parameters:
		shape_order (3) : Refinement order for the iconosphere.
			Number of facets is 20*(4**shape_order)
	
	Result:
		Sphere surface mesh (pymesh object). Has attributes
		vertices, faces, num_vertices, num_faces, dim, vertex_per_face, bbox
	"""

	return pymesh.generate_icosphere(1.0,[0.,0.,0.],shape_order)

#END generate_sphere


def to_volume_grid(mesh, res):
	"""
	Voxelizes the surface mesh into cubical voxels with give size.
	
	Input:
		mesh : Pymesh surface mesh object to be voxelized
		res : Sidelength of the cubical voxel
	
	Output:
		Numpy n*3 matrix of x,y,z integer coordinates of the cubical mesh
	"""

	global __res, __mini, __maxi, __ni
	
	__res=res
	__mini = np.int32(np.floor(mesh.bbox[0]/__res))
	__minx = __mini[0] * __res
	__maxi = np.int32(np.ceil(mesh.bbox[1]/__res))
	__ni = __maxi-__mini+1
	print(" [volume grid is "+str(__ni[0])+"x"+str(__ni[1])+"x"+str(__ni[2])+" voxels]")

	# Loop through polygons and build z-buffer projected to yz-plane
	# Init z-buffergrid
	zbufgrid = [[[] for j in range(__ni[2])] for i in range(__ni[1])]
	# Direction vector
	udir = np.array([1.0,0.0,0.0])
	# Loop
	for face in mesh.faces:
		abc = mesh.vertices[face]
		# Find circumsrcibing rectangle in yz-plane
		minigridyz = np.array([np.amin(abc[:,[1,2]],axis=0),np.amax(abc[:,[1,2]],axis=0)]).transpose()
		minigridij = np.array([
			[_y2i_low(minigridyz[0,0]), _y2i_high(minigridyz[0,1])],
			[_z2j_low(minigridyz[1,0]), _z2j_high(minigridyz[1,1])]
		])
		# z-buffer for values in minigrid
		for i in range(minigridij[0,0],minigridij[0,1]):
			y = _i2y(i)
			for j in range(minigridij[1,0],minigridij[1,1]):
				z = _j2z(j)
				[c, intest] = _intersect_triangle_3d(np.array([__minx,y,z]), udir, abc[0], abc[1], abc[2])
				if (intest):
					zbufgrid[i][j].append(c)
	
	# For building voxel list
	vox = []
	xran = list(range(__ni[0]))
	xpen = np.array(xran) * __res
	ones = np.ones(__ni[0])
	# Sort z-buffer and build voxel list
	for i in range(__ni[1]):
		for j in range(__ni[2]):
			zbufgrid[i][j] = np.unique(np.array(zbufgrid[i][j]).round(decimals=12)).tolist()
			bufsum = np.zeros(__ni[0],dtype=np.int8)
			for zval in zbufgrid[i][j]:
				bufsum += np.array([int(b) for b in xpen>zval])
			xis = list(compress(xran,bufsum % 2 == 1))
			for x in xis:
				vox.append([x,i,j])

	return vox

#END to_volume_grid


def volume(mesh, vres=0.1):
	"""
	Computes volume of the mesh particle. Volume is
	computed via tetrahedralization of the surface mesh, and
	this volume mesh object is also returned.
	
	Input:
		mesh : Surface mesh
		vres (0.1) : maximum tetrahedral cell size
	
	Output:
		[volume, volume mesh object]
	"""
	
	vmesh = pymesh.tetrahedralize(mesh, vres)
	vmesh.add_attribute("voxel_volume")
	vol = np.sum(vmesh.get_attribute("voxel_volume"))
	
	return [vol, vmesh]

#END volume


###############################################
# Private functions


def _boundary_faces(T):

	T1 = np.array([T[:,0], T[:,1],T[:,2]]) 
	T2 = np.array([T[:,0], T[:,1],T[:,3]])
	T3 = np.array([T[:,0], T[:,2],T[:,3]]) 
	T4 = np.array([T[:,1], T[:,2],T[:,3]])

	T  = np.concatenate((T1,T2,T3,T4),axis=1)
	T = np.sort(T,axis=0)

	unique_cols, inverse = np.unique(T,axis=1, return_inverse=True)
	counts = np.bincount(inverse)==1
	F = unique_cols[:,counts] 

	return F.transpose()

#END _boundary_faces


def _coef_std(a_l, beta, l, m, cind):
	"""
	Determine the standard deviation from the autocorrelation
	"""

	std = beta*np.sqrt(a_l)
	for i in cind:
		j = int(l[i]*(l[i]+1)/2)
		std[i] = std[j]*np.sqrt(2.*factorial(l[i]-m[i])/factorial(l[i]+m[i]))

	return std
   
#END _coef_std


def _corr(x,ell):
	"""
	Gaussian correlation for ellipsoid
	"""
	
	if(x/ell<10.):
		return np.exp(-0.5*x**2/ell**2)	
	return 0.0

#END _corr


def _c_ind(n, m=None):
	"""
	For vectorizing we want to, well, use vectors. Here we handle indexing between
	arrays and vectors (both ways).
	"""

	if(m==None):
		nn = np.floor((np.sqrt(np.asarray(n)*8+1)-1)/2)
		nn = nn.astype(int)
		return nn, n-(nn**2+nn)/2
	nn = np.asarray(n)*(np.asarray(n)+1)/2+np.asarray(m)

	return nn

#END _c_ind


def _deform(sphere, a_lm, b_lm, beta, lmin, lmax):
	"""
	Generate the Gaussian deformation using the spherical harmonics expansion
	"""

	vertices = 0.0*sphere.nodes
	# Node coordinates:
	for i in range(sphere.num_nodes):
		z = sphere.nodes[i,2]
		phi = np.arctan2(sphere.nodes[i,1],sphere.nodes[i,0])
		r = _r_gsphere(a_lm, b_lm, z, phi, beta, lmin, lmax)
		nu = np.sqrt(1.0-z**2)
		vertices[i,:] = [r*nu*np.cos(phi),r*nu*np.sin(phi),r*z]

	return vertices

#END _deform


def _deform_mesh(mesh, ell, h, beta):
	"""
	Compute the deformation of surface normals
	"""
	
	hn = _deform_surf(mesh, ell, h, beta)

	mesh.add_attribute("vertex_normal")
	nn = mesh.get_vertex_attribute("vertex_normal")
	X = np.zeros((mesh.num_vertices,3))
	# Node coordinates:
	for i in range(mesh.num_vertices):
		X[i,:] = mesh.vertices[i,:] + (hn[i]-h)*nn[i,:]
		 
	return X

#END _deform_mesh


def _deform_surf(mesh, ell, h, beta):

	n = mesh.num_vertices
	cv = np.diag(np.ones(mesh.num_vertices))

	for i in range(n):
		for j in range(n):
			d = la.norm(mesh.vertices[i,:]-mesh.vertices[j,:])
			cv[i,j] = _corr(d, ell)
			cv[j,i] = cv[i,j]

	h1 = _rand_gauss(cv,n)  
	hn = h*np.exp(beta*h1-0.5*beta**2)
		
	return hn

#END _deform_surf


def _fix_mesh(mesh, detail="normal"):

    bbox_min, bbox_max = mesh.bbox;
    diag_len = norm(bbox_max - bbox_min);
    if detail == "normal":
        target_len = diag_len * 2e-2;
    elif detail == "high":
        target_len = diag_len * 10e-3;
    elif detail == "low":
        target_len = diag_len * 3e-2;

    count = 0;
    mesh, __ = pymesh.remove_degenerated_triangles(mesh, 100);
    mesh, __ = pymesh.split_long_edges(mesh, target_len);
    num_vertices = mesh.num_vertices;
    while True:
        mesh, __ = pymesh.collapse_short_edges(mesh, 1e-6);
        mesh, __ = pymesh.collapse_short_edges(mesh, target_len,
                preserve_feature=True);
        mesh, __ = pymesh.remove_obtuse_triangles(mesh, 150.0, 100);
        if mesh.num_vertices == num_vertices:
            break;

        num_vertices = mesh.num_vertices;
        print(" [fixing mesh, no. of vertices: {}]".format(num_vertices));
        count += 1;
        if count > 10: break;

    mesh = pymesh.resolve_self_intersection(mesh);
    mesh, __ = pymesh.remove_duplicated_faces(mesh);
    mesh = pymesh.compute_outer_hull(mesh);
    mesh, __ = pymesh.remove_duplicated_faces(mesh);
    mesh, __ = pymesh.remove_obtuse_triangles(mesh, 179.0, 5);
    mesh, __ = pymesh.remove_isolated_vertices(mesh);

    return mesh;

#END _fix_mesh


def _genellip(a, b, c, n):

	nodes = _sphere_points(n)

	elements = ch(nodes).simplices
	elements = _surface_fix(nodes, elements)
	M = np.diag([a,b,c])
	nodes = np.dot(M,nodes.T).T
	ellipsoid = pymesh.form_mesh(nodes,elements)

	return ellipsoid

#END _genellip


def _i2y(i):

	global __res, __mini

	return (i+__mini[1])*__res

#END _i2y


def _intersect_triangle_3d(ori, dir, a, b, c):
	"""
	Find the intersection point of line and triangle in 3d.
	
	Modified from Möller and Trumbore algorithm by BrunoLevy in StackOverflow.
	Note, counts intesections from both front- and backside directions of polygons. 
	
	Input:
		ori Origin of the line
		dir Direction of the line
		a,b,c Vertices of the triangle
	
	Output:
		Distance c that gives the intersection point as ori + c*dir, and boolean flag for intersection
	"""

	e1 = b-a
	e2 = c-a
	n = np.cross(e1,e2)
	det = -np.dot(dir, n)
	invdet = 1.0/det
	ao = ori - a
	dao = np.cross(ao, dir)
	u = np.dot(e2,dao) * invdet
	v = -np.dot(e1,dao) * invdet
	t = np.dot(ao,n) * invdet

	return [t, np.abs(det) >= 1e-6 and t >= 0.0 and u >= 0.0 and v >= 0.0 and (u+v) <= 1.0]

#END _intersect_triangle_3d


def _j2z(j):

	global __res, __mini

	return (j+__mini[2])*__res

#END _j2z


def _mesh_vectors(V,F):
	"""
	Creates a vector set of the mesh data for nice plotting.
	"""
	
	msh = np.zeros((np.shape(F)[0],3,3))
	for i, face in enumerate(F):
		for j in range(3):
			msh[i][j] = V[face[j],:]

	return msh

#END _mesh_vectors


def _mgaussian_lcoef(l, ell):
	a_l = 2.0*(l+1)*spherical_in(l,1.0/ell**2)*np.exp(-1.0/ell**2)
	a_l[0:lmin+1]=0.
	norm = np.sum(a_l)

	return a_l/norm
   
#END _mgaussian_lcoef


def _power_lcoef(l, nu, lmin, lmax):
	"""
	# Define power law and gaussian autocorrelation functions
	"""

	a_l = np.zeros(l.size)
	for i,value in enumerate(l):
		if(value!=0):
			a_l[i] = 1.0/value**nu
	a_l[0:lmin+1]=0
	norm = np.sum(1.0/np.arange(lmin,lmax+1)**nu)

	return a_l/norm

#END _power_lcoef


def _rand_gauss(cv,n):
	"""
	Generate Gaussian random deviates.
	"""

	D = np.zeros((n,n))

	# Note: cv is assumed to be positive-definite
	eigval, eigvec = la.eigh(cv) 

	# In ascending order and drop everything ill-conditioned
	e = eigval[::-1]
	v = np.fliplr(eigvec)
	v[:,e<0.] = 0.0
	e[e<0.] = 0.0

	for j in range(n):
		for i in range(n):
			D[i,j] = np.sqrt(cv[i,i]*e[j])*v[i,j]

	rn = normal(size=n)

	return np.dot(D,rn)

#END _rand_gauss


def _r_gsphere(a_lm, b_lm, z, phi, beta, lmin, lmax):
	"""
	Find the radius functions of the g-sphere
	"""
	
	if(lmax==0):
		return a_lm[0,0]
	legp, _ = lpmn(lmax, lmax, z)
	CPHI = np.cos(np.arange(0,lmax+1)*phi)
	SPHI = np.sin(np.arange(0,lmax+1)*phi)
	s = 0.
	for ll in range(lmin,lmax):
		ii = int(_c_ind(ll,0))
		s = s + legp[ll,0]*a_lm[ii]
	for mm in range(1,lmax):
		for ll in range(max(mm,lmin),lmax):    
			ii = int(_c_ind(ll,mm))
			s = s + legp[mm,ll]*(a_lm[ii]*CPHI[mm]+b_lm[ii]*SPHI[mm])
	
	return np.exp(s-0.5*beta**2)

#END _r_gsphere


def _sample_from_sphere(n=1, r=1, d=3):
	"""
	Random points inside d-dimensional sphere.
	"""
	
	di = 1/d;
	res = np.array([u**di *  r*x/np.linalg.norm(x) for [x,u] in \
		zip(np.random.normal(size=[n,d]), np.random.uniform(size=n))])
	
	return res

#END _sample_from_sphere


def _sample_gaussian_sphere_coef(std, l, lmin, lmax):
	"""
	The spherical harmonics representation of the G-sample-sphere from the 
	statistics
	"""
	a_lm = normal()*std
	b_lm = np.zeros(std.size)
	for mm in range(1,lmax):
		for ll in range(max(mm,lmin),lmax):
			i = int(_c_ind(ll,mm))
			a_lm[i] = normal()*std[i]
			b_lm[i] = normal()*std[i]
			
	return a_lm, b_lm
	
#END _sample_gaussian_sphere_coef


def _sphere_points(n=1000):

	x = np.zeros([3,n])
	offset = 2./n
	increment = np.pi * (3. - np.sqrt(5.));

	for i in range(n):
		yi = ((i * offset) - 1) + (offset / 2);
		r = np.sqrt(1 - yi**2)
		phi = ((i + 1) % n) * increment

		x[:,i] = [np.cos(phi) * r, yi, np.sin(phi) * r]

	return x.T
 
#END _sphere_points


def _surface_fix(V,F):

	n = np.zeros([3,np.shape(F)[0]])
	centroids = np.zeros([3,np.shape(F)[0]])
	for i, face in enumerate(F):
		n[:,i] = np.cross(V[face[1],:]-V[face[0],:],V[face[2],:]-V[face[0],:])/2.
		centroids[:,i] = (V[face[0],:] + V[face[1],:] + V[face[2],:])/3
		if np.dot(n[:,i], centroids[:,i]) < 0:
			f0 = face[0]; f1 = face[1]; f2 = face[2]
			F[i,0] = f2
			F[i,1] = f1
			F[i,2] = f0
		 
	return F

#END _surface_fix


def _y2i_high(y):

	global __res, __mini, __ni
	
	return min(int(np.ceil(y/__res))-__mini[1],__ni[1])

#END _y2i_high


def _y2i_low(y):

	global __res, __mini
	
	return max(int(np.floor(y/__res))-__mini[1],0)

#END _y2i_low


def _z2j_high(z):

	global __res, __mini, __ni
	
	return min(int(np.ceil(z/__res))-__mini[2],__ni[2])

#END _z2j_high


def _z2j_low(z):

	global __res, __mini
	
	return max(int(np.floor(z/__res))-__mini[2],0)

#END _z2j_low



###############################################
# Private variables

__res = 0.1
__ni = np.array([10,10,10])
__mini = np.array([0,0,0])
__maxi = np.array([10,10,10])

